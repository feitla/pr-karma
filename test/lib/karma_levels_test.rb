require 'test_helper'

class KarmaLevelsTest < ActiveSupport::TestCase
  test "translates into levels, non-expanded range" do
    dataset = KarmaEvent.where(repo_name: 'repo_bravo').handles_and_balances
    kl = KarmaLevels.new(dataset)

    assert_equal [["alex", :high_development],
                  ["john", :low_development],
                  ["mary", :slightly_review]], kl.handles_and_levels
  end

  test "translates into levels, expanded range" do
    dataset = KarmaEvent.where(repo_name: 'repo_alpha').handles_and_balances
    kl = KarmaLevels.new(dataset)

    assert_equal [["developer", :high_development], ["otherppl", :slightly_review]], kl.handles_and_levels
  end

  test "translates a single balance" do
    kl = KarmaLevels.new('dudebro', 15)

    assert_equal [["dudebro", :low_review]], kl.handles_and_levels
  end

  test "handles an empty dataset" do
    kl = KarmaLevels.new([])
    assert_equal [], kl.handles_and_levels
  end
end
