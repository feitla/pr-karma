require 'test_helper'

class PullRequestReviewCommentHandlerTest < ActiveSupport::TestCase
  test 'happy path PR review comment submitted' do
    json = payload_mock 'github_pull_request_review_comment'

    handler = PullRequestReviewCommentHandler.new(json)

    assert_difference ->{ KarmaEvent.count } => 1 do
      handler.handle
    end

    assert_nil handler.slack_message

    ke = KarmaEvent.last
    assert_equal 1, ke.adjustment
    assert_equal 'review_comment', ke.event
    assert_equal 'prksandbox', ke.repo_name
    assert_equal 'mrinterweb', ke.handle
    assert_equal 'Update README.md', ke.pr_title
    assert_equal 147033853, ke.review_id
    assert_equal 208947793, ke.pr_id
  end

  test 'does not reward if comment author == PR author' do
    json = payload_mock 'github_pull_request_review_comment_same_author'

    handler = PullRequestReviewCommentHandler.new(json)

    assert_difference ->{ KarmaEvent.count } => 0 do
      handler.handle
    end

    assert_nil handler.slack_message
  end

  test 'does not reward if the comment is a followup comment' do
    json = payload_mock 'github_pull_request_review_followup_comment'

    handler = PullRequestReviewCommentHandler.new(json)

    assert_difference ->{ KarmaEvent.count } => 0 do
      handler.handle
    end

    assert_nil handler.slack_message
  end
end
