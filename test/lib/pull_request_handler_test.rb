require 'test_helper'

class PullRequestHandlerTest < ActiveSupport::TestCase
  test 'happy path github test' do
    json = payload_mock 'github_pull_request'

    handler = PullRequestHandler.new(json)

    assert_difference -> { KarmaEvent.count } => 1 do
      handler.handle
    end

    assert_equal "mcnelson has gone from *In balance* to *Development I* " +
                 "for opening new pull request 'Update README.md'.", handler.slack_message

    ke = KarmaEvent.last
    assert_equal -9, ke.adjustment
    assert_equal 'pull_request_created', ke.event
    assert_equal 'prksandbox', ke.repo_name
    assert_equal 'mcnelson', ke.handle
    assert_equal 'Update README.md', ke.pr_title
    assert_equal 208938717, ke.pr_id
  end

  test 'ignores event if the repo name is blacklisted' do
    ENV['IGNORED_REPO_NAMES'] = 'foobar'

    json = payload_mock 'github_pull_request_ignored_repo'

    handler = PullRequestHandler.new(json)

    assert_difference -> { KarmaEvent.count } => 0 do
      handler.handle
    end

    assert_nil handler.slack_message
  end
end
