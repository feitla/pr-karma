ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/mock'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # WTF??? Not the default behavior!??!?!!!?
  self.use_transactional_tests = true

  # Add more helper methods to be used by all tests here...
  def payload_mock(name)
    name << '.json' unless name.ends_with?('.json')
    pl = File.read(Rails.root.join('test', 'payload_mocks', name))
    JSON.parse(pl)
  end

  def json
    @json ||= JSON.parse(response.body, object_class: OpenStruct)
  end

  def after_teardown
    if !passed? && respond_to?(:response) && response.present?
      puts JSON.pretty_generate JSON.parse(response.body)
    end

  rescue JSON::ParserError
    nil
  end
end
