require 'test_helper'

class LevelsControllerTest < ActionDispatch::IntegrationTest
  test 'fetches current balances as levels' do
    get polymorphic_url([:levels]), as: :json

    assert_response :success

    assert_kind_of Array, json
    assert_equal "alex", json.first.handle
    assert_equal "medium_development", json.first.level
    assert_equal -0.64, json.first.karmaPosition

    assert_equal "otherppl", json.last.handle
    assert_equal "slightly_review", json.last.level
    assert_equal 0.14, json.last.karmaPosition
  end
end
