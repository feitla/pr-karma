require 'test_helper'

class KarmaEventTest < ActiveSupport::TestCase
  test "disallows duplicate approvals within an hour" do
    # TODO: Extract to commmon attributes
    attrs = {repo_name: 'bla', adjustment: 1}

    KarmaEvent.create!(attrs.merge(event: :review_approved,
                                   created_at: 30.minutes.ago,
                                   pr_id: 1,
                                   handle: 'foo'))

    e = KarmaEvent.new(attrs.merge(event: :review_approved,
                                   pr_id: 1,
                                   handle: 'foo'))

    assert e.invalid?
  end
end
