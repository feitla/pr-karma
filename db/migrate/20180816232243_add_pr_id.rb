class AddPrId < ActiveRecord::Migration[5.2]
  def change
    change_table :karma_events do |t|
      t.integer :pr_id, null: true, index: true
    end
  end
end
