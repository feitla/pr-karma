class AddReviewKarmaEventId < ActiveRecord::Migration[5.2]
  def change
    change_table :karma_events do |t|
      t.references :review_karma_event, null: true, index: true
    end
  end
end
