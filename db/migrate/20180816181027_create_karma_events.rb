class CreateKarmaEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :karma_events do |t|
      t.string :handle, null: false
      t.string :event, null: false
      t.integer :adjustment, null: false

      t.string :repo_name, null: false

      t.string :pr_title, null: true
      t.integer :review_id, null: true, index: true

      t.timestamps null: false
    end
  end
end
