class PullRequestReviewCommentHandler < KarmaEventHandler
  def handle
    if json.action == 'created' && !was_author_of_pr? && !followup_comment?
      perform_adjustment! json.comment.user.login,
                          1,
                          :review_comment,
                          json.pull_request,
                          review_id: json.comment.pull_request_review_id
    end
  end

  private

  def was_author_of_pr?
    json.comment.user.login == json.pull_request.user.login
  end

  # Github pings us when someone replies to a PR review. If that's the case, it's a followup comment
  # rather than an original review comment.
  def followup_comment?
    json.comment.in_reply_to_id
  end
end
