class KarmaEventHandler
  include SlackPingable
  attr_reader :payload, :json, :repo_name, :event

  def self.handle(payload)
    new(payload).
      handle
  end

  def initialize(payload)
    @payload = payload
    @json = JSON.parse(payload.to_json, object_class: OpenStruct)

    @repo_name = json.pull_request.head.repo.name
  end

  def perform_adjustment_with_ping(*args)
    return unless preprocess_event
    @event = persist_adjustment(*args)
    ping_slack
    log_karma_event

    @event
  end

  def perform_adjustment_with_ping!(*args)
    return unless preprocess_event
    args.last[:raise_on_invalid] = true
    @event = persist_adjustment(*args)
    ping_slack
    log_karma_event

    @event
  end

  def perform_adjustment!(*args)
    return unless preprocess_event
    args.last[:raise_on_invalid] = true
    @event = persist_adjustment(*args)
    log_karma_event

    @event
  end

  def perform_adjustment(*args)
    return unless preprocess_event
    @event = persist_adjustment(*args)
    log_karma_event

    @event
  end

  def ping_slack
    return if event.invalid?
    slack.ping change.build_change_notification
  end

  def preprocess_event
    return false if ignored_repo?
    LevelsChannel.broadcast_to("levels", {}) # TODO: Add new data
  end

  private

  def ignored_repo?
    repo_name.in?(ENV.fetch('IGNORED_REPO_NAMES', '').split(','))
  end

  def persist_adjustment(handle, adjustment, event, pull_request_object, attrs = {})
    method_name = attrs.delete(:raise_on_invalid) ? :create! : :create

    KarmaEvent.public_send(method_name, {repo_name: repo_name,
                                         handle: handle,
                                         adjustment: adjustment,
                                         event: event,
                                         pr_id: pull_request_object.id,
                                         pr_title: pull_request_object.title}.merge(attrs))
  end

  def change
    @change ||= begin
                  KarmaLevelChange.new(event.event,
                                       event.adjustment,
                                       event.handle,
                                       event.pr_title)
                end
  end

  def log_karma_event
    return if event.invalid?
    old_level, new_level = change.karma_level_change

    Rails.logger.info('[adjustment] event_id=%d handle=%s change=%d new=%s old=%s balance=%d' % [
      event.id,
      event.handle,
      event.adjustment,
      old_level,
      new_level,
      KarmaEvent.balance(event.handle)])
  end
end
