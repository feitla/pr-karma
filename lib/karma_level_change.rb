class KarmaLevelChange
  attr_reader :event, :adjustment, :handle, :pr_title, :extras

  # adjustment = -6
  # handle = mcnelson
  def initialize(event, adjustment, handle, pr_title, extras = {})
    @event = event
    @adjustment = adjustment
    @handle = handle
    @pr_title = pr_title
    @extras = extras
  end

  def build_change_notification
    old_level, new_level = karma_level_change

    result_description = I18n.t(new_level == old_level ? :remains : :changed,
                                scope: :slack_notifications,
                                old_level: old_level,
                                new_level: new_level)

    I18n.t event, result_description: result_description,
                  adjustment: adjustment,
                  handle: handle,
                  pr_title: pr_title,
                  comment_count_appendage: comment_count_appendage,
                  scope: :slack_notifications,
                  raise: true
  end

  def karma_level_change
    new_balance = KarmaEvent.balance(handle)
    old_level = KarmaLevels.new(handle, new_balance - adjustment).level
    new_level = KarmaLevels.new(handle, new_balance).level

    [I18n.t(old_level, scope: :karma_levels, raise: true),
     I18n.t(new_level, scope: :karma_levels, raise: true)]
  end

  private

  def comment_count_appendage
    return '' if event != 'review_submitted'
    I18n.t('slack_notifications.comment_count_appendage',
           comment_count: extras.fetch(:comment_count),
           comment_word: I18n.t(:comment_word).pluralize(extras.fetch(:comment_count)),
           raise: true)
  end
end
