module SlackPingable
  class DummyClient
    attr_reader :last_slack_message

    class ConfigContext
      def defaults(_)
      end
    end

    def initialize(_, &block)
      ConfigContext.new.instance_eval(&block)
    end

    def ping(m)
      @last_slack_message = m
      puts "SLACK: #{m}"
    end
  end

  def slack_message
    slack.last_slack_message
  end

  private

  def slack_client
    Rails.env.production? ? Slack::Notifier : DummyClient
  end

  def slack
    @slack ||= slack_client.new(webhook_url) do
      defaults channel: "#pr-karma",
               username: "karmabot",
               icon_emoji: ":crossed_fingers:"
    end
  end

  def webhook_url
    ENV.fetch('SLACK_WEBHOOK_URL').presence or raise 'SLACK_WEBHOOK_URL must be set'
  end
end
