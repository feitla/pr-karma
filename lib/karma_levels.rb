class KarmaLevels
  PERCENTILES_INTO_LEVEL_KEYS = {
    -0.25 => :slightly_development,
    -0.5 => :low_development,
    -0.75 => :medium_development,
    -1.0 => :high_development,

    0.25 => :slightly_review,
    0.5 => :low_review,
    0.75 => :medium_review,
    1.0 => :high_review
  }.freeze

  # Starting range before it can expand to accomodate extremes.
  STANDARD_RANGE = 30.freeze

  attr_reader :handles_and_balances, :handles_and_proportions

  # Pass [["john", -10], ["mary", 3]] or "dudebro", 15
  def initialize(*handles_and_balances)
    if handles_and_balances.first.is_a?(Array)
      @handles_and_balances = handles_and_balances.first
    else
      @handles_and_balances = [handles_and_balances]
    end

    @handles_and_proportions = {}
  end

  def level
    raise 'use #handles_and_levels for more than 1 translation' if handles_and_balances.length > 1
    handles_and_levels.first.last
  end

  def handles_and_levels
    sort translate_into_levels(handles_and_balances)
  end

  private

  def sort(pairs)
    pairs.sort_by! { |(handle, _)| handle.downcase }
  end

  def translate_into_levels(handles_and_balances)
    range_size = fetch_range_size

    handles_and_balances.map do |(handle, balance)|
      # Edge case: Range size is just slightly behind current balance
      range_size = balance if balance.abs > range_size

      @handles_and_proportions[handle] = balance / range_size.to_f

      percentile = [0.25, 0.5, 0.75, 1.0].bsearch do |b|
        @handles_and_proportions[handle].abs <= b
      end

      percentile = -percentile if @handles_and_proportions[handle].negative?

      [handle, PERCENTILES_INTO_LEVEL_KEYS.fetch(percentile)]
    end
  end

  def fetch_range_size
    [fetch_current_highest_score, STANDARD_RANGE].max
  end

  def fetch_current_highest_score
    KarmaEvent.
      current.
      group(:handle).
      pluck(Arel.sql("abs(sum(adjustment))")).max
  end
end
