class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.simple_enum_strings(*values)
    values.each.with_object({}) { |item, h| h[item] = item }
  end
end
