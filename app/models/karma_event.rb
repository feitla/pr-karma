class KarmaEvent < ApplicationRecord
  # Do not count approvals within this timeframe per user, per PR.
  APPROVAL_TIMEFRAME = 1.hour.freeze

  # Karma events must be within this timeframe to be considered "current"
  CURRENT_KARMA_TIMEFRAME = 1.month.freeze

  has_many :review_comments, class_name: 'KarmaEvent', foreign_key: "review_karma_event_id"
  belongs_to :review_event, class_name: "KarmaEvent", optional: true

  validate :one_approval_per_timeframe, on: :create, if: :review_approved?
  enum event: simple_enum_strings('pull_request_created', 'pull_request_retracted', 'review_submitted',
                                  'review_comment', 'review_approved')
  def self.current
    where(created_at: CURRENT_KARMA_TIMEFRAME.ago..Time.now)
  end

  def self.balance(handle)
    current.where(handle: handle).sum(:adjustment)
  end

  def self.handles_and_balances
    group(:handle).pluck(Arel.sql("handle, SUM(adjustment)"))
  end

  def already_approved_recently?
    self.class.where(event: event,
                     created_at: APPROVAL_TIMEFRAME.ago..Time.now,
                     pr_id: pr_id,
                     handle: handle).exists?
  end

  private

  def one_approval_per_timeframe
    errors.add(:base, 'recently approved this PR') if already_approved_recently?
  end
end
