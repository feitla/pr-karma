class LevelsChannel < ApplicationCable::Channel
  def subscribed
    stream_for "levels"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
