class GithubHooksController < ApplicationController
  include GithubWebhook::Processor

  def github_pull_request(payload)
    PullRequestHandler.new(payload).handle
    head :ok
  end

  def github_pull_request_review(payload)
    PullRequestReviewJob.set(wait: 10.seconds).perform_later(payload)
    head :ok
  end

  def github_pull_request_review_comment(payload)
    PullRequestReviewCommentHandler.new(payload).handle
    head :ok
  end

  private

  def webhook_secret(payload)
    ENV.fetch('GITHUB_WEBHOOK_SECRET').presence or raise 'GITHUB_WEBHOOK_SECRET not set'
  end
end
