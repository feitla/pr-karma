class LevelsController < ApplicationController
  # TODO: Move to a parent controller for api resources
  before_action do
    unless request.remote_ip == ENV.fetch('ALLOWED_IP') || !Rails.env.production?
      render status: :forbidden
    end
  end

  def index
    balances = KarmaEvent.current.handles_and_balances

    kl = KarmaLevels.new(balances)
    @levels = kl.handles_and_levels
    @proportions = kl.handles_and_proportions
  end
end
