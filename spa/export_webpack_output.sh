#!/bin/sh

mv dist/css/app*.css ../public/app.css
mv dist/js/app*.js ../public/spa.js
mv dist/js/chunk-vendors*.js ../public/chunk-vendors.js
