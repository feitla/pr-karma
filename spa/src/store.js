import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
Vue.prototype.$store = Vuex

const store = new Vuex.Store({
  state: () => {
    return {
      levels: []
    }
  },
  getters: {
    levels: state => state.levels
  },
  mutations: {
    levels (state, value) {
      state.levels = value
    }
  }
})

export default store
