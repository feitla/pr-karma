Rails.application.routes.draw do
  resource :github_hook, only: :create, defaults: {formats: :json}, path: 'github-hook'
  resources :levels
end
